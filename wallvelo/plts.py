import os
from glob import glob
import numpy as nup
import io
def nupToFile(f,ar): # I have not found an easyer way to do this :/
    bio = io.BytesIO()
    nup.savetxt(bio,ar,newline=' ')
    f.write(bio.getvalue().decode('latin1')+'\n')

def plotShape(shape,np=100):
    #First let's create a vtk file with some points
    try:
        f = open('shape.vtk','x')
    except IOError:
        ans = askSth("File shape.vtk already exists...\n"+
                "[o]verwrite or save [b]ackup?",['o','b'])
        if ans==0:
            f = open('shape.vtk','w')
        else:
            fils = glob('shape.vtk')
            fils.sort(key=len)
            nfil = fils[-1]+'.bak'
            os.rename('shape.vtk',nfil)
            f = open('shape.vtk','x')

    #Write header to file
    f.write('# vtk DataFile Version 2.0\n')
    f.write('Shape\n')
    f.write('ASCII\n')
    f.write('DATASET UNSTRUCTURED_GRID\n')
    f.write('POINTS '+str(np)+' float\n')
    t_lin = nup.linspace(0.,1.,num=np)
    bio = io.BytesIO()
    for t in t_lin:
        nupToFile(f,shape.arc(t))
    return

def askSth(msg,opt):
    ans = input(msg)
    try:
        return opt.index(ans.lower())
    except ValueError:
        return askSth('Answer '+ans+' not valid. Answer with ' + ' '.join(opt),opt)



