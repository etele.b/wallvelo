class Poly():
    #Takes a path definition from an svg file and converts it to the form:
    # / m x1,y1 x2,y2 x3,y3 \
    # | m
    #
    #
    #
    #
    #
    #
    #
    #
    commands = 'mcz'

    def __init__(self,dstring):
        import re
        from math import ceil
        self.defi = []
        coms = re.findall('['+self.commands+'][\s\d,])',dstring)
        err = 1
        for com in coms:
            if com[0] == 'm':
                self.defi.append('m')
            elif com[0] == 'c':
                ic = float(len(vals)/3)
                if ic.isinteger():
                    for i in range(ic):
                        self.defi.append(('c',[j.split(',') for j in a[i*3:i*3+2]]))
            elif com[0] == 'z':
                self.defi.append('z')
            else:
                print('Oops, something went wrong with the path definition')
                return -1
            err = 0
        return err

    def arc(self,t):
        if t<0 or t>1:
            print('Parameter t out of bounds...')
            return -1
        np = len(self.defi)
        for i in range(np):
            if i/np <= t <= (i+1)/np:
                if self.defi[i][0] == 'm':
                    #Interpolate polygon - linear
                    linp(t-i/np,self.defi[i][1:2])
                elif self.defi[i][0] == 'c':
                    qbez(t-i/np,self.defi[i][1:4])
                elif self.defi[i][0] == 'z':
                    #Go home
                    linp(t-i/np,[self.defi[i-1][-1],self.defi[0][1]])
                else:
                    print('Oooops, this shouldn\'t have happened...')
                    return -2
        return 1

    @staticmethod
    def qbez(t,p):
        return (1-t)^3*p[0]+3*(1-t)^2*t*p[1]+3*(1-t)*t^2*p[2]+t^3*p[3]

    @staticmethod
    def linp(t,p):
        return (t-1)*p[0]+t*p[1]

