class Boundary():
    def __init__(self,gfil,sfil,pfil):
        self.gfil = gfil
        self.sfil = sfil
        self.pfil = pfil

    def loadShape(self):
        from wallvelo.shape import Shape
        #Maybe also switch to the shape namespace?
        self.Shape = Shape(self.sfil)

