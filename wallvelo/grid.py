import numpy as nup
class Grid():
    def __init__(self, nx = 500, ny = 1000 ):
        self.nx = nx
        self.ny = ny
        self.data = nup.zeros( shape = ( nx, ny ), dtype = float)
        self.offx = 0
        self.offy = 0
        pass

    def addRow(self,ne,elements,mag,offsx=0,offsy=0):
        # Add row to current poisition, with the grid-elements
        # defined in elements, each with its magnitude
        self.offx = self.offx + offsx
        self.offy = self.offy + offsy
        for i in range(ne):
            #Check if in bounds...
            if self.offx + elements[i].nx <= self.nx and \
                    self.offy + elements[i].ny <= self.ny:
                        self.data[ self.offx:self.offx+elements[i].nx,\
                                self.offy:self.offy+elements[i].ny ] \
                                = elements[i].data*mag[i]
                        self.offx += elements[i].nx
            else:
                break
        self.offx = 0
        self.offy += elements[i].ny
        return





