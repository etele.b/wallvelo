import numpy as nup
class gridElement():
    def __init__( self, shape , nx = 50, ny = 50):
        self.shape = shape
        self.nx = nx
        self.ny = ny
        self.data = nup.zeros((nx, ny), dtype=float)
        self.allp = pSet()
        self.xx = nup.linspace( 0, 1, nx )
        self.yy = nup.linspace( 0, 1, ny )
        for i in range(nx):
            for j in range(ny):
                self.allp.add([ nup.array([ i, j ]) ])


        self.inp = self.allp.inside( self.shape, self.xx, self.yy )
        pass

    def initV(self,mag=1):
        self.setData(self.inp,mag/len(self.inp))
        return

    def divRegions( self ):
        self.reg = []
        self.reg.append(( self.allp / self.inp ).hasneighbour( self.inp ))
        remaining = self.inp
        r = 0
        while True:
            newreg = remaining.hasneighbour( self.reg[-1] )
            if len(newreg) > 0:
                r += 1
                self.reg.append( newreg )
                remaining = remaining / newreg
            else:
                break
        self.rmax = r
        return

    def setData( self, points, val ):
        for i in points.index:
            self.data[ i[0], i[1] ] = val

    def linfilter( self, r , val = 1 ):
        if r > self.rmax:
            r = self.rmax

        s = 0

        for ri in range( 1, r ):
            s += ri * len(self.reg[ri])

        rii = ri
        while rii < self.rmax+1:
            s += ri * len(self.reg[rii])
            rii += 1

        m = val/s

        for ri in range( 1, r ):
            self.setData( self.reg[ ri ], m*ri )

        rii = ri
        while rii < self.rmax+1:
            self.setData( self.reg[ rii ], m*ri )
            rii += 1

        return


class pSet():

    def __init__(self):
        self.index = nup.zeros( (0, 2), dtype = int )
        return

    def add( self, ix ):
        self.index = nup.append( self.index, ix, axis = 0 )

    def __truediv__( self, other ):
        ret = pSet()
        for i in self.index:
            keep = True
            for j in other.index:
                if i[0] == j[0] and i[1] == j[1]:
                    keep = False
                    break
            if keep:
                ret.add([ i ])
        return ret

    def __add__( self, other ):
        ret = pSet()
        ret.add( self.index )
        ret.add( (other / self).index )
        return ret

    def __len__( self ):
        return len( self.index )

    def hasneighbour( self, other ):
        ret = pSet()
        for i in self.index:
            for j in other.index:

                if (i[0] == j[0] and abs(i[1] - j[1]) == 1) or (i[1] == j[1] \
                        and abs(i[0] - j[0]) == 1):
                    ret.add([ i ])
        return ret

    def inside( self, shape ,xx, yy ):
        ret = pSet()
        for i in self.index:

            if shape.isInside([ xx[ i[0] ], yy[ i[1] ] ]):
                ret.add([ i ])
        return ret


