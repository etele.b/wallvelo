import numpy as nup
import re
import math


class Shape():
    def __init__(self):
        self.commands = 'CcMmZzVvHhLl'

    @classmethod
    def retdef(cls,text,pat1,pat2):
        # Return properties in pat2 in the object pat1 in the svg file
        # e.g.: text: "<rect style="5" id="someid"/> <path style="bla" d="blah"/>
        #       pat1: 'rect'
        #       pat2: ['style','id']
        # would return [5, 'someid']
        props = []
        try:
            obj = re.search('<'+pat1+'(.+?)/>',text,flags=re.DOTALL).group(1)
            for pat in pat2:
                props.append(re.search(pat+'\"(.+?)\"',obj,flags=re.DOTALL).group(1))
        except AttributeError:
            print("Oooops: Object "+pat1+"not found")
            return -1
        return props

    def readShape(self,sfil):
        try:
            with open(sfil,'r') as f:
                # Find boundary (rectangle)
                text = f.read()
                temp = ['x=','y=','width=','height=']
                self.dom = Shape.retdef(text,'rect',temp)
                if self.dom==-1:
                    print("Something went wrong while reading domain in file...")
                    print(sfil)
                    return -1
                print('Domain definition found:')
                print('x='+self.dom[0]+'-'+self.dom[2]+'; y='+self.dom[1]+'-'+self.dom[3])
                self.pstring = Shape.retdef(text,'path',['d='])

                self.orig = nup.array([self.dom[0],self.dom[1]],dtype=float)
                self.range = nup.array([self.dom[2],self.dom[3]],dtype=float)
                self.orig[1] = self.orig[1]-self.range[1]
                if self.pstring==-1:
                    print("Something went wrong while reading path definition in file...")
                    print(self.sfil)
                    return -1
                else:
                    print("Path definition found: "+self.pstring[0])
                    return 1
        except IOError:
            print("Could not open file: "+self.sfil)

    def addpoi(self,p1,sp,flip=False):
        r = nup.zeros(2)
        r[0] = p1[0] + nup.array(sp.split(',')).astype(float)[0]
        r[1] = p1[1] + nup.array(sp.split(',')).astype(float)[1]
        return r

    def polInit(self):
        from math import ceil
        from operator import add
        self.defi = []
        coms = re.findall('['+self.commands+'][\s\d,-.]*',self.pstring[0])
        err = 1
        first = True
        citer = iter(coms)
        while True:
            try:
                com = next(citer)
                poi = iter(com[1:].split())
                if first:
                    cp = self.addpoi(-self.orig,next(poi))
                    fp = cp
                    first = False
                if com[0] == 'm' or com[0] == 'l':
                    while True:
                        try:
                            p1 = self.addpoi(cp,next(poi))
                            self.defi.append(['m',nup.array([cp,p1])])
                            cp = p1
                        except StopIteration:
                            break
                elif com[0] == 'M' or com[0] == 'L':
                    while True:
                        try:
                            p1 = self.addpoi(-self.orig,next(poi))
                            self.defi.append(['m',nup.array([cp,p1])])
                        except StopIteration:
                            break
                elif com[0] == 'c':
                    while True:
                        try:
                            p0 = cp
                            p1 = self.addpoi(cp,next(poi),True)
                            p2 = self.addpoi(cp,next(poi),True)
                            p3 = self.addpoi(cp,next(poi),True)
                            cp = p3
                            self.defi.append(['c',nup.array([p0,p1,p2,p3])])
                        except StopIteration:
                            break
                elif com[0] == 'C':
                    while True:
                        try:
                            p0 = cp
                            p1 = self.addpoi(-self.orig,next(poi))
                            p2 = self.addpoi(-self.orig,next(poi))
                            p3 = self.addpoi(-self.orig,next(poi))
                            cp = p3
                            self.defi.append(['c',nup.array([p0,p1,p2,p3])])
                        except StopIteration:
                            break
                elif com[0] == 'H':
                    p1 = nup.array([float(next(poi))-self.orig[0],\
                            cp[1]])
                    self.defi.append(['m',nup.array([cp,p1])])
                    cp = p1
                elif com[0] == 'h':
                    p1 = nup.array([float(next(poi))+cp[0],\
                            cp[1]])
                    self.defi.append(['m',nup.array([cp,p1])])
                    cp = p1
                elif com[0] == 'V':
                    p1 = nup.array([cp[0],\
                            float(next(poi))+self.orig[1]])
                    self.defi.append(['m',nup.array([cp,p1])])
                    cp = p1
                elif com[0] == 'v':
                    p1 = nup.array([cp[0],\
                            float(next(poi))+cp[1]])
                    self.defi.append(['m',nup.array([cp,p1])])
                    cp = p1
                elif com[0].lower() == 'z':
                    self.defi.append(['m',nup.array([cp,fp])])
                else:
                    print('Oops, something went wrong with the path definition')
                    return -1
            except StopIteration:
                break
        err = 0
        return err

    def arc(self,t):
        if t<0 or t>1:
            print('Parameter t out of bounds...')
            return -1
        np = len(self.defi)
        for i in range(np):
            if i/np <= t <= (i+1)/np:
                if self.defi[i][0] == 'm':
                    #Interpolate polygon - linear
                    r = linp((t-i/np)*np,self.defi[i][1])/self.range
                elif self.defi[i][0] == 'c':
                    r = qbez((t-i/np)*np,self.defi[i][1])/self.range
                elif self.defi[i][0] == 'z':
                    #Go home
                    r = linp((t-i/np)*np,self.defi[i][1])
                else:
                    print('Oooops, this shouldn\'t have happened...')
                    return -2
        r[1] = 2-r[1]
        return r
    def isInside(self,p,res=10):
        t = nup.linspace(0.,1.,num=res)
        a = 0
        for i in range(0,res-1):
            vi = self.arc(t[i])
            vii = self.arc(t[i+1])
            if vi[1] <= p[1]:
                if vii[1] > p[1]:
                    if isLeft(vi, vii, p) > 0:
                        a += 1
            else:
                if vii[1] <= p[1]:
                    if isLeft(vi, vii, p) < 0:
                        a -= 1
        return a != 0





def qbez(t,p):
    return ((1-t)**3)*p[0]+3*((1-t)**2*t)*p[1]+3*((1-t)*t**2)*p[2]+t**3*p[3]
def linp(t,p):
    return (1-t)*p[0]+t*p[1]

def dotp(v1,v2):
    return sum(v1*v2)

def isLeft(p0,p1,p2):
    return (p1[0]-p0[0])*(p2[1]-p0[1])-\
            (p2[0]-p0[0])*(p1[1]-p0[1])
