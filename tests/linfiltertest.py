#/usr/bin/env python
from context import gE
from context import Shape
import numpy as nup
import random

b = Shape()
b.readShape('tests/shapetest.svg')
b.polInit()
a = gE.gridElement(b)

import matplotlib.pyplot as plt

a.divRegions()
input('r = '+str(a.rmax))
a.initV()
a.linfilter(10)
plt.contourf(a.xx, a.yy , a.data)
plt.show()
