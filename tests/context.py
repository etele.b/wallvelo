import os
import sys

sys.path.insert(0,os.path.abspath(os.path.join(os.path.dirname(__file__),'..')))

from wallvelo.shape import Shape
import wallvelo.gridElement as gE
from wallvelo.grid import Grid
