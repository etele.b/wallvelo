from context import gE
from context import Shape
from context import Grid
import numpy as nup
import random

# Initialize shapes
s1 = Shape()
s2 = Shape()

# Load shape from SVG file
s1.readShape('tests/s1.svg')
s2.readShape('tests/s2.svg')

# Initialize polygon
s1.polInit()
s2.polInit()

# Now are shapes are finished

# Let's initialize some grid elements with it
g1 = gE.gridElement(s1)
g2 = gE.gridElement(s2)

# we need to divide grid element into regions to filter them

g1.divRegions()
g2.divRegions()

# now let's initialize a velocity field for the holes in the
# grid element (not neccesary)

g1.initV()
g2.initV()

# apply linear filter on one of them (with the radius 10)

g1.linfilter(10)
g2.linfilter(10)

# Not let's define a grid for the U and the V components with default values
uG = Grid()
vG = Grid()

# and add some rows at the begining of the U grid  with the addrow function

uG.addRow(10,[ g1, g1, g1, g1, g1, g1, g1, g1, g1, g1],\
        [ 1., 1., 1., 1., 1., 1., 1., 1, 1., 1.])
uG.addRow(10,[ g2, g2, g2, g2, g2, g2, g2, g2, g2, g2],\
        [ -1., -1., -1., -1., -1., -1., -1., -1, -1., -1.],
        25)
uG.addRow(10,[ g2, g2, g2, g2, g2, g2, g2, g2, g2, g2],\
        [ 1., 1., 1., 1., 1., 1., 1., 1, 1., 1.])
uG.addRow(10,[ g1, g1, g1, g1, g1, g1, g1, g1, g1, g1],\
        [ -1., -1., -1., -1., -1., -1., -1., -1, -1., -1.])

import matplotlib.pyplot as plt
plt.contourf(uG.data)
plt.show()
