#/usr/bin/env python
from context import Shape
import numpy as nup

b = Shape()
b.readShape('tests/shapetest.svg')
b.polInit()
import matplotlib.pyplot as plt

np = 100
t = nup.linspace(0.,1.,num=np)
sh = nup.zeros((np,2),float)
for i in range(0,np):
    sh[i,:] = b.arc(t[i])

plt.scatter(sh[:,0],sh[:,1],c='b')
plt.xlim(0,1)
plt.ylim(0,1)
for i in nup.linspace(0,1,20):
    for j in nup.linspace(0,1,20):
        p = nup.array([i,j])
        ins = b.isInside(p)
        if ins:
            col = 'g'
        else:
            col = 'r'
        plt.scatter(p[0],p[1],c=col)


plt.show()
