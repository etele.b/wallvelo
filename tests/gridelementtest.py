#/usr/bin/env python
from context import gE
from context import Shape
import numpy as nup
import random

b = Shape()
b.readShape('tests/shapetest.svg')
b.polInit()
a = gE.gridElement(b)


import matplotlib.pyplot as plt

a.divRegions()
r = 0.1
for region in a.reg:
    pxx, pyy = gE.gridPoint(a.xx,a.yy,region.index)
    r = random.random()
    plt.scatter(pxx,pyy,c=(r,1-r,r))
#plt.scatter(p1xx,p1yy,c='r')
plt.xlim(0,1)
plt.ylim(0,1)
plt.show()
